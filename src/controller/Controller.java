package controller;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	/**
	 * forma de conectarse con la clase que imprime informaci�n a la consola
	 */
	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	/**
	 * M�todo constructor, inicializa los atributos de la clase
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}

	/**
	 * m�todo en el que se corre el programa.
	 */
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				this.loadMovingViolations();
				break;

			case 2:
				IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
				view.printDailyStatistics(dailyStatistics);
				break;

			case 3:
				view.printMensage("Ingrese el número de infracciones a buscar");
				int n = sc.nextInt();

				IStack<VOMovingViolations> violations = this.nLastAccidents(n);
				view.printMovingViolations(violations);
				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}


	/**
	 * M�todo que se encarga de cargar los datos tanto al stack como a la cola
	 * post: los datos se han cargado al programa
	 */
	public void loadMovingViolations() {
		try {
			movingViolationsStack = new Stack<VOMovingViolations>();
			movingViolationsQueue = new Queue<VOMovingViolations>();
			FileReader nm = new FileReader("./data/Moving_Violations_Issued_In_January_2018_ordered.csv");

			CSVReader reader = new CSVReader(nm);
			String nextLine[];
			nextLine = reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				movingViolationsStack.push(new VOMovingViolations(nextLine));
				movingViolationsQueue.enqueue(new VOMovingViolations(nextLine));
			}
			reader.close();
			nm = new FileReader("./data/Moving_Violations_Issued_In_February_2018_ordered.csv");
			reader = new CSVReader(nm);
			nextLine = reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				movingViolationsStack.push(new VOMovingViolations(nextLine));
				movingViolationsQueue.enqueue(new VOMovingViolations(nextLine));
			}
			reader.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que retorna las estadisticas diarias de accidentes con una cola 
	 * @return estadisticas diarias en una cola de tipo VODayLyStatistic
	 */
	public IQueue <VODaylyStatistic> getDailyStatistics () {
		Queue<VODaylyStatistic> est = new Queue<VODaylyStatistic>();
		int fine = 0; int nA = 0; int nI = 0; String date = "";
		boolean primera = true;
		for (VOMovingViolations actual : movingViolationsQueue) {
			if(primera) {
				fine = actual.getfineAMT(); nA = 0; nI = 0; date = actual.getTicketIssueDate().split("T")[0];
				primera = false;
			}
			if(!date.equals(actual.getTicketIssueDate().split("T")[0]) ) {
				est.enqueue(new VODaylyStatistic(date, nA, nI, fine));
				fine = actual.getfineAMT(); nA = 0; nI = 0; date = actual.getTicketIssueDate().split("T")[0];
			}
			if(actual.getAccidentIndicator().equals("Yes"))
				nA++;
			nI++;
			fine += actual.getfineAMT();
		}
		est.enqueue(new VODaylyStatistic(date, nA, nI, fine));
		return est;	
	}

	/**
	 * M�todo que retorna los ultimos n accidentes en una pila de VOMovingViolations
	 * @param n la cantidad de los accidentes deseados n > 0
	 * @return los ultimos n accidentes en una pila de VOMovingViolations
	 */
	public IStack <VOMovingViolations> nLastAccidents(int n) {
		Stack<VOMovingViolations> answer = new Stack<VOMovingViolations>();
		int contador = 0;
		for(VOMovingViolations violaciones: movingViolationsStack) {
			if(violaciones.getAccidentIndicator().equals("Yes")) {
				answer.push(violaciones);
				contador++;
			}
			if(contador == n)
				break;
		}
		return answer;
	}
}
