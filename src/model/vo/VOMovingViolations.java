package model.vo;

import java.sql.RowId;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	/**
	 * ObjectID del VOMovingViolations.
	 */
	private int objectID;

	/**
	 * Row del VOMovingViolations.
	 */
	private String ROW;

	/**
	 * Location del VOMovingViolations.
	 */
	private String location;

	/**
	 * AdressID del VOMovingViolations.
	 */
	private String adressID;
	
	/**
	 * StreetSegid del VOMovingViolations.
	 */
	private String streetSegid;

	/**
	 * XCOORD del VOMovingViolations.
	 */
	private double XCOORD;

	/**
	 * YCOORD del VOMovingViolations.
	 */
	private double YCOORD;

	/**
	 * TicketType del VOMovingViolations.
	 */
	private String ticketType;

	/**
	 * FineAMT del VOMovingViolations.
	 */
	private int fineAMT;

	/**
	 * TotalPaid del VOMovingViolations.
	 */
	private int totalPaid;

	/**
	 * Penalty1 del VOMovingViolations.
	 */
	private int penalty1;

	/**
	 * Penalty2 del VOMovingViolations.
	 */
	private int penalty2;

	/**
	 * AccidentIndicator del VOMovingViolations.
	 */
	private String accidentIndicator;
	
	/**
	 * TicketIssueDate del VOMovingViolations.
	 */
	private String ticketIssueDate;

	/**
	 * ViolationCode del VOMovingViolations.
	 */
	private String violationCode;

	/**
	 * ViolationDescription del VOMovingViolations.
	 */
	private String violationDescription;

	/**
	 * ROWID del VOMovingViolations.
	 */
	private String ROWID;

	/**
	 * Constructor del VOMovingViolations.
	 * @param pLine Linea para construir el VOMovingVIolations
	 */
	public VOMovingViolations( String[] pLine) {
		objectID = Integer.parseInt(pLine[0]);
		ROW = pLine[1];
		location = pLine[2];
		adressID = pLine[3];
		streetSegid = pLine[4];
		XCOORD = Double.parseDouble(pLine[5]);
		YCOORD = Double.parseDouble(pLine[6]);
		ticketType = pLine[7];
		fineAMT = Integer.parseInt(pLine[8]);
		totalPaid = Integer.parseInt(pLine[9]);
		penalty1 = Integer.parseInt(pLine[10]);
		try{
			penalty2 = Integer.parseInt(pLine[11]);
		}
		catch(Exception e){
			penalty2 = 0;
		}
		accidentIndicator = pLine[12];
		ticketIssueDate = pLine[13];
		violationCode = pLine[14];
		violationDescription = pLine[15];
		ROWID = pLine[16];
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		return objectID;
	}	

	/**
	 * @return Row del VOMovingViolations.
	 */
	public String getROW() {
		return ROW;
	}


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return AdressID del VOMovingViolations.
	 */
	public String getAdressID(){ 
		return adressID;
	}

	/**
	 * @return StreetSgid del VOMovingViolations.
	 */
	public String getStreetSgid() {
		return streetSegid;
	}
	
	/**
	 * @return XCOORD del VOMovingViolations.
	 */
	public double getXCOORD(){
		return XCOORD;
	}

	/**
	 * @return YCOORD del VOMovingViolations.
	 */
	public double getYCOORD(){ 
		return YCOORD;
	}

	/**
	 * @return TicketType del VOMovingViolations.
	 */
	public String ticketType(){ 
		return ticketType;
	}

	/**
	 * @return FineAMT del VOMovingViolations.
	 */
	public int getfineAMT(){
		return fineAMT;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return Penalty1 del VOMovingViolations.
	 */
	public int penalty1(){ 
		return penalty1;
	}

	/**
	 * @return Penalty2 del VOMovingViolations.
	 */
	public int penalty2(){ 
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return violationCode - Devuelve el c�digo de violaci�n.
	 */
	public String getViolationCode() {
		return violationCode;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		return violationDescription;
	}

	/**
	 * @return ROWID del VOMovingViolations.
	 */
	public String getROWID(){ 
		return ROWID;
	}

}
