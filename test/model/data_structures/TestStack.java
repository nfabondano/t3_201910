package model.data_structures;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.fail;

import org.junit.Before;
import junit.framework.TestCase;

/**
 * Clase usada para verificar si la Pila esta implementada correctamente.
 */
public class TestStack extends TestCase{
	
	/**
	 * Pila sobra la que se realizan las pruebas.
	 */
	private Stack<String> pilaString;
	
	/**
	 * Escenario 1: Crea una pila vac�a
	 */
	public void setupEscenario1() {
		pilaString = new Stack<String>();
	}

	/**
	 * Escenario 2: Crea una pila con 6 objetos tipo string.
	 */
	public void setupEscenario2() {
		pilaString = new Stack<String>();

		pilaString.push("Sexto");
		pilaString.push("Quinto");
		pilaString.push("Cuarto");
		pilaString.push("Tercero");
		pilaString.push("Segundo");
		pilaString.push("Primero");
	}
	
	/**
	 * Prueba 1: Verifica que size est� bien implementado
	 */
	public void testSize() {
		setupEscenario1();
		assertTrue("Deber�a ser 0", pilaString.size() == 0);
		setupEscenario2();
		assertTrue("Deber�a ser 6", pilaString.size() == 6 );
	}
	
	/**
	 * Prueba 2: Verifica que push este bien implementado
	 */
	public void testPush() {
		setupEscenario1();
		pilaString.push("Primero");
		pilaString.push("Segundo");
		assertTrue("El tama�o deber�a ser 2", pilaString.size() == 2);
	}
	
	/**
	 * Prueba 3: Verifica que isEmpty este bien implementado
	 */
	public void testIsEmpty() {
		setupEscenario1();
		assertTrue("No aparece vac�o", pilaString.isEmpty());
		setupEscenario2();
		assertTrue("Aparece vac�o", !pilaString.isEmpty() );
	}
	
	/**
	 * Prueba 4: Verifica que pop este bien implementado
	 */
	public void testPop() {
		setupEscenario2();
		assertTrue("No se elimino correctamente", pilaString.pop().equals("Primero"));
		assertTrue("No se conto correctamente la eliminaci�n", pilaString.size() == 5);
	}
	
	/**
	 * Prueba 5: Verifica que iterator este bien implementada
	 */
	public void testIterator() {
		setupEscenario1();
		Iterator<String> it = pilaString.iterator();
		assertTrue("hasNext() no funciona correctamente", !it.hasNext());
		setupEscenario2();
		it = pilaString.iterator();
		assertTrue("hasNext() no funciona correctamente", it.hasNext());
		assertTrue("next() no funciona correctamente", it.next().equals("Primero"));
		assertTrue("next() no funciona correctamente", it.next().equals("Segundo"));
		setupEscenario1();
		for(int i = 0; i < 8; i++) {
			pilaString.push("" + i);
		}
		int m = 7;
		for(String nume: pilaString) {
			assertTrue("el iterador no devuelve la pila en el orden correcto", nume.equals(("" +m)));
			m--;
		}
	}
	
}
